//
//  ViewController.swift
//  PlayingSound
//
//  Created by Md Qayyum Shareef on 05/06/18.
//  Copyright © 2018 Md Qayyum Shareef. All rights reserved.
//

import UIKit
import AVFoundation

class ViewController: UIViewController {
    
    var soundEffect: AVAudioPlayer = AVAudioPlayer()
    var musicEffect: AVAudioPlayer = AVAudioPlayer()

    @IBOutlet weak var volumeSlider: UISlider!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        let  soundFile = Bundle.main.path(forResource: "Sound", ofType: "wav")
        let musicFile = Bundle.main.path(forResource: "Music", ofType: "mp3")
        
        do {
            
            soundEffect = try AVAudioPlayer(contentsOf: URL(fileURLWithPath: soundFile!))
            try musicEffect = AVAudioPlayer(contentsOf: URL(fileURLWithPath: musicFile!))
        } catch {
            print(error)
        }
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func playSound(_ sender: Any) {
        soundEffect.play()
        
    }
    
    
    @IBAction func playMusic(_ sender: Any) {
        musicEffect.play()
    }
    
    @IBAction func pause(_ sender: Any) {
        musicEffect.pause()
    }
    
    @IBAction func restart(_ sender: Any) {
        musicEffect.currentTime = 0
    }
    
    @IBAction func stopMusic(_ sender: Any) {
        musicEffect.stop()
        musicEffect.currentTime = 0
    }
    
    @IBAction func volume(_ sender: Any) {
        soundEffect.volume = volumeSlider.value
        musicEffect.volume = volumeSlider.value
        
    }
    
    
    
}







